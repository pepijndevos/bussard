# Bussard credits

Original code, art, and prose copyright © 2015-2016 Phil Hagelberg and contributors
https://gitlab.com/technomancy/bussard/graphs/master

Distributed under the GNU General Public License version 3 or later; see LICENSE

## 3rd-party code

* Lume library copyright © 2015 rxi, distributed under the MIT license
  https://github.com/rxi/lume
* globtopattern copyright © 2011-2012 David Manura, distributed under the MIT license
  https://github.com/davidm/lua-glob-pattern
* md5.lua copyright © 2013 Enrique García Cota + Adam Baldwin +
    hanzao + Equi 4 Software, distributed under the MIT license
  https://github.com/kikito/md5.lua
* serpent Copyright © 2011-2013 Paul Kulchenko, distributed under the MIT license
  https://github.com/pkulchenko/serpent
* utf8.lua Copyright © 2016 Stepets, distributed under the MIT license
  https://github.com/Stepets/utf8.lua
* l2l Copyright © 2012-2015, Eric Man and contributors, distributed under 2-clause BSD license
  https://github.com/meric/l2l

## 3rd-party art

* Some planet graphics copyright © 2013 Rawdanitsu, CC0 licensed
  http://opengameart.org/content/planets-and-stars-set-high-res

* Other planet graphics copyright © 2013 GM Shaber, CC-BY licensed
  http://opengameart.org/content/27-planets-in-hi-res

* Solar system graphics copyright © 2013 Inove, CC-BY licensed
  http://www.solarsystemscope.com/nexus/resources/planet_images/

* Station graphics copyright © 2014 MillionthVector, CC-BY licensed
  https://millionthvector.blogspot.de/p/free-sprites_12.html

* Asteroid graphics copyright © 2014 para, CC0 licensed
  http://opengameart.org/content/low-poly-rocks

* Inconsolata font copyright © 2006 Raph Levien
  Released under the SIL Open Font License
  https://www.google.com/fonts/specimen/Inconsolata

* Deja Vu Sans Mono © 2004-2016 DejaVu fonts team
  Released under Bitstream Vera Fonts Copyright
  https://dejavu-fonts.github.io/

## 3rd-party prose

* Lua Programming book copyright © 2013-2016 Mark Otaris and Wikibooks contributors,
  distributed under the Creative Commons Attribution-ShareAlike license
  https://en.wikibooks.org/wiki/Lua_Programming

## Influences

* Escape Velocity (gameplay)
* Kerbal Space Program (mechanics)
* Marathon Trilogy (story)
* A Fire upon the Deep (story)
* Anathem (story, philosophy)
* Mindstorms (philosophy)
* GNU Emacs (architecture)
* Unix (architecture)
* Atomic Rockets (science)
* Planescape: Torment (story, gameplay)
* Meditations on Moloch (philosophy)
