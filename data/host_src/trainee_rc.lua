record_event("trainee01")

print(
"You made it to the terminal, nice work. You're now logged in to a shell\n"..
"session in the Orb operating system. You can log out of this session by\n"..
"running `logout` and then log out of the rover with the same.\n")

print(
"Once your application is reviewed, you will receive an email from CMEC\n"..
"recruiting with further instructions.")

