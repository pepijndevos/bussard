local mail = require("mail")
local mission = require("mission")

local coro = coroutine.create(function(ship)
      while(ship.system_name ~= "Wolf 294") do coroutine.yield() end
      mail.deliver_msg(ship, "dex19-memory-2.msg")
      mission.record_credentials(ship, "Solotogo", "nari02", "mendaciouspolyglottal")
end)

return {
   init = function(ship) table.insert(ship.updaters, coro) end,
   name = "Access memory card",
   description = "Take the memory card to Nari on Solotogo (Wolf 294 system)" ..
      " to find out what it says.",

   objectives={"memory_card_delivered"},
   on_success = function(ship)
      mail.deliver_msg(ship, "nari-memory-01.msg")
      -- mail.deliver_msg(ship, "nari-memory-02.msg")
   end,
}
