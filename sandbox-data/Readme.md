# Bussard Sandbox data

This is another set of data files for Bussard which allow you to just explore
the universe without having the limitations of the main story bog you down.

Run this command to use it:

    $ love . --data sandbox-data
