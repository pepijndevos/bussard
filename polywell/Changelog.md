# 0.3.0 / ???

* Fix completion for opening files off disk.
* Support reloading polywell from inside polywell.
* Screen splits (choose from 3 hard-coded layouts).
* Rudimentary Clojure mode and nrepl client.
* Add polywell.coroutines for background tasks.
* Support setting mouse cursors on a per-mode basis.
* Allow console's loadstring function to be customized.
* Support drawing minibuffer with custom draw function.
* Better support for drawing non-textual buffers.

# 0.2.0 / 2016-09-13

* Rename event handler functions to match LÖVE naming.
* Write reference manual.
* Add `with_output_to` function to redirect `print` and `write`.
* Add mouse handlers.
* Allow modes to define their own wrap functions for custom undo.
* Add Emacs-style prefix maps for key bindings.

# 0.1.0 / 2016-09-04

* Initial release as a standalone project.
