# smolforth

Only suitable for entertainment/educational purposes, obviously!

Watch and giggle as someone who once implemented Forth three years ago
tries again from memory, but in a different language.

## License

Copyright © 2017 Phil Hagelberg and contributors

Distributed under the GNU General Public License version 3 or later; see file LICENSE.
